<?php

namespace Drupal\translated_config;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableDependencyTrait;

/**
 * Class TranslatedImmutableConfig.
 *
 * @package Drupal\translated_config
 */
class TranslatedImmutableConfig implements CacheableDependencyInterface {
  
  use CacheableDependencyTrait;

  /**
   * Configuration Array with all merged values.
   *
   * @var array
   */
  private $config = [];

  /**
   * TranslatedImmutableConfig constructor.
   *
   * @param array $config
   *   The entire configuration dataset.
   */
  public function __construct(array $config, CacheableDependencyInterface $cacheability) {
    $this->config = $config;
    $this->setCacheability($cacheability);
  }

  /**
   * Get a key from the translated configuration object.
   *
   * @param string $key
   *   The configuration values you want to fetch. Seperated by dots.
   *
   * @return array|null
   *   The given configuration, with overwritten translated values.
   */
  public function get($key = '') {
    if (empty($key)) {
      return $this->config;
    }
    $keys = preg_split('/\./', $key);
    $value = $this->config;
    foreach ($keys as $key) {
      if (!empty($key) && is_array($value) && array_key_exists($key, $value)) {
        $value = $value[$key];
      }
      else {
        return NULL;
      }
    }
    return $value;
  }

}
