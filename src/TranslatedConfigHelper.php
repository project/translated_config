<?php

namespace Drupal\translated_config;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Class TranslatedConfigHelper.
 *
 * @package Drupal\translated_config
 */
class TranslatedConfigHelper {

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Language Manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;

  /**
   * TranslatedConfigHelper constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config Factory.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Language Manager.
   */
  public function __construct(ConfigFactoryInterface $configFactory, LanguageManagerInterface $languageManager) {
    $this->configFactory = $configFactory;
    $this->languageManager = $languageManager;
  }

  /**
   * Get the translated configuration set.
   *
   * This configuration set is complete with all keys that the original language
   * has to offer. Every key that has a translation will have the translated
   * value in its place. Merging is done using array_replace_recursive().
   *
   * @param string $configName
   *   The name of the configuration file.
   * @param string $langCode
   *   The language id. Leave empty for current Drupal language.
   *
   * @return \Drupal\translated_config\TranslatedImmutableConfig
   *   Returns the combined translated configuration as an object.
   */
  public function getTranslatedConfig($configName, $langCode = NULL) {
    if (empty($langCode)) {
      $langCode = $this->languageManager->getCurrentLanguage()->getId();
    }
    $originalConfig = $this->configFactory->get($configName);
    $translatedConfig = $this->languageManager->getLanguageConfigOverride($langCode, $configName);
    $config = array_replace_recursive($originalConfig->get(), $translatedConfig->get());
    $cacheability = new CacheableMetadata();
    $cacheability
      ->addCacheableDependency($originalConfig)
      ->addCacheableDependency($translatedConfig);
    return new TranslatedImmutableConfig($config, $cacheability);
  }

}
